rust-tree-sitter-cli (0.20.7-3) unstable; urgency=medium

  * Team upload.
  * Gate test code that requires 64-bit atomics behind
    #[cfg(target_has_atomic = "64")]
  * Stop overriding debian/tests/control, edit Cargo.toml instead.
  * Add patch to deal with char signedness issue in test.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 22 Feb 2023 01:30:17 +0000

rust-tree-sitter-cli (0.20.7-2) experimental; urgency=medium

  * Package tree-sitter-cli 0.20.7 from crates.io using debcargo 2.6.0
  * Add emscripten to Recommends for "tree-sitter build-wasm"
  * Update relax-deps.patch to allow building against rust-webbrowser 0.8

 -- James McCoy <jamessan@debian.org>  Mon, 20 Feb 2023 20:05:47 -0500

rust-tree-sitter-cli (0.20.7-1) experimental; urgency=medium

  * Package tree-sitter-cli 0.20.7 from crates.io using debcargo 2.6.0
    (Closes: #1029745)
  * New patches
    + relax-deps: Allow use of pretty-assertions 1
    + skip-tests-using-fixtures: Add "#[ignore]" attribute to tests which
      rely on unpackaged fixtures
    + writable-scratch-dir: Use CARGO_TARGET_DIR or CARGO_TARGET_TMPDIR as the
      SCRATCH_DIR for tests, so autopkgtests have a writable directory to use
  * Disable benchmarks since they rely on unpackaged fixtures

 -- James McCoy <jamessan@debian.org>  Thu, 09 Feb 2023 23:32:51 -0500
