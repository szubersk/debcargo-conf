This patch is based on the upstream commit described below, adapted for use
in the Debian package by Peter Michael Green.

commit 7936e3513ee339650a7ac2eff11edd2fac3f037d
Author: Nora Widdecke <nora@sequoia-pgp.org>
Date:   Thu Sep 8 17:49:08 2022 +0200

    openpgp, sq: Update dependency rpassword to 6.
    
      - rpassword underwent some rework. The successor of
        read_password_from_tty seems to be prompt_password, relevant commits
        to rpassword:
          - e6023757df00a67a1e16796db50c5ffad41b6268
          - 2edf6cee07573ec4aa86531e6177ee90331d5c60

Index: sequoia-sq/src/commands/decrypt.rs
===================================================================
--- sequoia-sq.orig/src/commands/decrypt.rs
+++ sequoia-sq/src/commands/decrypt.rs
@@ -254,10 +254,11 @@ impl<'a> DecryptionHelper for Helper<'a>
                         break keypair;
                     }
 
-                    let p = rpassword::read_password_from_tty(Some(
-                        &format!(
-                            "Enter password to decrypt key {}: ",
-                            self.key_hints.get(keyid).unwrap())))?.into();
+                    let p = rpassword::prompt_password(&format!(
+                        "Enter password to decrypt key {}: ",
+                        self.key_hints.get(keyid).unwrap()
+                    ))?
+                    .into();
 
                     match key.unlock(&p) {
                         Ok(decryptor) => break decryptor,
@@ -308,10 +309,11 @@ impl<'a> DecryptionHelper for Helper<'a>
                         break keypair;
                     }
 
-                    let p = rpassword::read_password_from_tty(Some(
-                        &format!(
-                            "Enter password to decrypt key {}: ",
-                            self.key_hints.get(&keyid).unwrap())))?.into();
+                    let p = rpassword::prompt_password(&format!(
+                        "Enter password to decrypt key {}: ",
+                        self.key_hints.get(&keyid).unwrap()
+                    ))?
+                    .into();
 
                     if let Ok(decryptor) = key.unlock(&p) {
                         break decryptor;
@@ -336,9 +338,10 @@ impl<'a> DecryptionHelper for Helper<'a>
 
         // Finally, try to decrypt using the SKESKs.
         loop {
-            let password =
-                rpassword::read_password_from_tty(Some(
-                    "Enter password to decrypt message: "))?.into();
+            let password = rpassword::prompt_password(
+                "Enter password to decrypt message: ",
+            )?
+            .into();
 
             for skesk in skesks {
                 if let Some(sk) = skesk.decrypt(&password).ok()
Index: sequoia-sq/src/commands/key.rs
===================================================================
--- sequoia-sq.orig/src/commands/key.rs
+++ sequoia-sq/src/commands/key.rs
@@ -154,10 +154,10 @@ fn generate(config: Config, command: Key
     }
 
     if command.with_password {
-        let p0 = rpassword::read_password_from_tty(Some(
-            "Enter password to protect the key: "))?.into();
-        let p1 = rpassword::read_password_from_tty(Some(
-            "Repeat the password once more: "))?.into();
+        let p0 = rpassword::prompt_password(
+            "Enter password to protect the key: ")?.into();
+        let p1 = rpassword::prompt_password(
+            "Repeat the password once more: ")?.into();
 
         if p0 == p1 {
             builder = builder.set_password(Some(p0));
@@ -255,11 +255,9 @@ fn password(config: Config, command: Key
     let new_password = if command.clear {
         None
     } else {
-        let prompt_0 =
-            rpassword::read_password_from_tty(Some("New password: "))
+        let prompt_0 = rpassword::prompt_password("New password: ")
             .context("Error reading password")?;
-        let prompt_1 =
-            rpassword::read_password_from_tty(Some("Repeat new password: "))
+        let prompt_1 = rpassword::prompt_password("Repeat new password: ")
             .context("Error reading password")?;
 
         if prompt_0 != prompt_1 {
Index: sequoia-sq/src/commands/mod.rs
===================================================================
--- sequoia-sq.orig/src/commands/mod.rs
+++ sequoia-sq/src/commands/mod.rs
@@ -126,9 +126,9 @@ fn get_keys<C>(certs: &[C], p: &dyn Poli
             if let Some(secret) = key.optional_secret() {
                 let unencrypted = match secret {
                     SecretKeyMaterial::Encrypted(ref e) => {
-                        let password = rpassword::read_password_from_tty(Some(
+                        let password = rpassword::prompt_password(
                             &format!("Please enter password to decrypt {}/{}: ",
-                                     tsk, key)))
+                                     tsk, key))
                             .context("Reading password from tty")?;
                         e.decrypt(key.pk_algo(), &password.into())
                             .expect("decryption failed")
@@ -140,8 +140,8 @@ fn get_keys<C>(certs: &[C], p: &dyn Poli
                           .unwrap()));
                 continue 'next_cert;
             } else if let Some(private_key_store) = private_key_store {
-                let password = rpassword::read_password_from_tty(
-                    Some(&format!("Please enter password to key {}/{}: ", tsk, key))).unwrap().into();
+                let password = rpassword::prompt_password(
+                    &format!("Please enter password to key {}/{}: ", tsk, key)).unwrap().into();
                 match pks::unlock_signer(private_key_store, key.clone(), &password) {
                     Ok(signer) => {
                         keys.push(signer);
@@ -323,12 +323,12 @@ pub fn encrypt(opts: EncryptOpts) -> Res
     let mut passwords: Vec<crypto::Password> = Vec::with_capacity(opts.npasswords);
     for n in 0..opts.npasswords {
         let nprompt = format!("Enter password {}: ", n + 1);
-        passwords.push(rpassword::read_password_from_tty(Some(
+        passwords.push(rpassword::prompt_password(
             if opts.npasswords > 1 {
                 &nprompt
             } else {
                 "Enter password: "
-            }))?.into());
+            })?.into());
     }
 
     if opts.recipients.len() + passwords.len() == 0 {
Index: sequoia-sq/src/sq.rs
===================================================================
--- sequoia-sq.orig/src/sq.rs
+++ sequoia-sq/src/sq.rs
@@ -265,12 +265,11 @@ fn decrypt_key<R>(key: Key<key::SecretPa
             let mut first = true;
             loop {
                 // Prompt the user.
-                match rpassword::read_password_from_tty(
-                    Some(&format!(
-                        "{}Enter password to unlock {} (blank to skip): ",
-                        if first { "" } else { "Invalid password. " },
-                        key.keyid().to_hex())))
-                {
+                match rpassword::prompt_password(&format!(
+                    "{}Enter password to unlock {} (blank to skip): ",
+                    if first { "" } else { "Invalid password. " },
+                    key.keyid().to_hex()
+                )) {
                     Ok(p) => {
                         first = false;
                         if p.is_empty() {
Index: sequoia-sq/Cargo.toml
===================================================================
--- sequoia-sq.orig/Cargo.toml
+++ sequoia-sq/Cargo.toml
@@ -47,7 +47,7 @@ features = ["derive", "env", "wrap_help"
 version = "0.10"
 
 [dependencies.rpassword]
-version = "5.0"
+version = "6.0"
 
 [dependencies.sequoia-autocrypt]
 version = "0.24"
